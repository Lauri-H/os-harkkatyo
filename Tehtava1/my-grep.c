/*
Nimi: Lauri Heininen
Opiskelija nro: 0429765
Harjoitusty� teht�v� 1

L�hteet:
https://stackoverflow.com/questions/2279379/how-to-convert-integer-to-char-in-c
https://www.tutorialspoint.com/c_standard_library/string_h.htm
https://www.tutorialspoint.com/c_standard_library/c_function_fwrite.htm
https://en.wikipedia.org/wiki/C_standard_library
Linux manuaali mm. strcpy
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define JAKAJAT " \t\a\n" 
#define riviPituus 2500

//Funktio esittelyt
void hakuKone(char*, char*);
void hakuStdin(char*);

int main(int argc, char* argv[])
{
	char nimi[30]; //Tiedoston nimi
	char haku[30]; //Haettava sana
	int i;

	//Virhek�sittely
	if(argc > 0 && argc < 2)
	{
		printf("my-grep: searchterm [file ...]\n");
		exit(0);
	}
	else if(argc == 2) //Jos pelkk� hakusana niin haetaan sy�tteest�
	{
		strcpy(haku, argv[1]);
		hakuStdin(haku);
	}
	else if(argc > 2) //Muuten haetaan tiedostosta
	{

		//K�yd��n kaikki annetut tiedostot l�pi
		for(i = 2; i< argc; i++)
		{
			strcpy(haku, argv[1]);
			strcpy(nimi,argv[i]);
			hakuKone(nimi, haku);
		}
	}
	else
	{
		printf("Tuntematon virhe\n");
	}
	
	return 0;

}

//Aliohjelma, joka hakee sanoja tiedostosta
void hakuKone(char* nimi, char* haku)
{
	//Avataan tiedosto
	FILE* tiedosto = fopen(nimi,"r");

	char rivi[riviPituus];
	char kokorivi[riviPituus];	
	char* sana; //Yksitt�inen sana
	char* kaikkisanat[2500]; //Lista kaikista sanoista
	int i = 0;
	int j;
	int onko; //Muuttuja vertailuun strcmp()
	
	//Tarkistetaan ett� tiedosto avautuu
	if(tiedosto == NULL)
	{
		printf("my-grep: cannot open file\n");
		exit(1);
	}	

	//Luetaan tiedosto rivi kerrallaan
	while((fgets(rivi, riviPituus,tiedosto)) != NULL)
	{
		//Pilkotaan luettu rivi palasiksi
		strcpy(kokorivi, rivi);
		sana = strtok(rivi, JAKAJAT);
		
		//Lis�t��n sanat listaan
		while(sana != NULL)
		{
			kaikkisanat[i] = sana;
			i++;
			sana = strtok(NULL, JAKAJAT);
		}		

		//Verrataan sana kerrallaan haettuun sanaan
		for (j=0; j<i; j++)
		{
			onko = strcmp(kaikkisanat[j], haku);
			
			if(onko == 0)
			{
				printf("%s\n", kokorivi);
				break;
			}
			
		}		
	
	}
	
	//Pari tyhj�� rivi� ja siivotaan 
	printf("\n\n");
	fclose(tiedosto);
	
}

//Aliohjelma, joka hakee sanoja sy�tteest�
void hakuStdin(char* haku)
{
	char rivi[riviPituus];
	char kokorivi[riviPituus];
	char* sana;
	char* kaikkisanat[250];
	int i = 0;
	int j;
	int onko;


	//Luetaan k�ytt�j�sy�te
	while(fgets(rivi, riviPituus, stdin) != NULL)
	{
		//Pilkotaan rivi sanoihin
		strcpy(kokorivi, rivi);
		sana = strtok(rivi, JAKAJAT);
		
		while(sana != NULL)
		{
			kaikkisanat[i] = sana;
			i++;
			sana = strtok(NULL, JAKAJAT);
		}		

		//Verrataan sanoja hakusanaan
		for (j=0; j<i; j++)
		{
			onko = strcmp(kaikkisanat[j], haku);
			
			if(onko == 0)
			{
				printf("%s\n", kokorivi);
				break;
			}
		}		
		
		if(j==i && onko != 0 && strcmp(kaikkisanat[0], "exit") != 0)
		{
			printf("Sanaa ei l�ydy\n");
		}

		if(strcmp(kaikkisanat[0], "exit") == 0)
		{
			exit(0);
		}
	}
	
	printf("\n\n");
}
