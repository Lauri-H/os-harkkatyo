/*
Nimi: Lauri Heininen
Opiskelija nro: 0429765
Harjoitusty� teht�v� 1

L�hteet:
https://stackoverflow.com/questions/2279379/how-to-convert-integer-to-char-in-c
https://www.tutorialspoint.com/c_standard_library/string_h.htm
https://www.tutorialspoint.com/c_standard_library/c_function_fwrite.htm
https://en.wikipedia.org/wiki/C_standard_library
Linux manuaali mm. strcpy ja getline
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define riviKoko 2500 //Luettavan tiedoston maksimi koko merkkein�

//Funktio esittely
void pakkaa(char* t_nimi, char* p_nimi);


int main(int argc, char* argv[])
{
	char* t_nimi; //Alkuper�inen tiedosto
	char* p_nimi; //Tiedosto johon pakataan

	//tarkistetaan ett� argumentteja on oikea m��r�
	if(argc < 3)
	{
		perror("Liian v�h�n argumentteja.\n");
	}
	else if(argc>3)
	{
		perror("Liikaa argumentteja");
	}
	else //Jos kaikki kunnossa niin aloitetaan
	{
		t_nimi = argv[1];
		p_nimi = argv[2];
	}
	
	pakkaa(t_nimi,p_nimi);
	
	return 0;
	
}

//Aliohjelma joka pakkaa tiedoston. Parametreina tiedostojen nimet
void pakkaa(char* t_nimi, char* p_nimi)
{
	int maara = 1; //Per�kk�isten samanlaisten kirjaimien m��r�
	int pituus; //Luettavan rivin pituus
	int i;
	int p_paikka = 0; //pakattavan tiedoston viimeinen kirjain
	char c_maara; //maara muutettuna char
	char ed_kirjain; 
	char seu_kirjain;
	char rivi[riviKoko]; //Luettava rivi
	char p_rivi[riviKoko]; //Pakattu tiedosto stringin�
	
	//Avataan molemmat tiedostot
	FILE* tek_tiedosto = fopen(t_nimi, "r");
	FILE* pak_tiedosto = fopen(p_nimi, "wb");
	
	//Luetaan niin kauan kuin tiedostossa on rivej�
	while(fgets(rivi, riviKoko, tek_tiedosto) != NULL)
	{
		pituus = strlen(rivi);
		
		//Suoritetaan niin kauan kuin rivi� on j�ljell�
		for(i= 1; i<=pituus; i++)
		{
			//Luetaan edellinen ja seuraava kirjain
			ed_kirjain = rivi[i-1];
			seu_kirjain = rivi[i];
			
			//Jos kirjaimet ovat samat lis�t��n m��r��
			if(ed_kirjain == seu_kirjain)
			{
				maara++;
			}
			else //Jos eiv�t ole niin kirjoitetaan
			{
				if(maara > 1) //Jos on useampi samanlainen per�kk�in lis�t��n numero
				{
					c_maara = maara+'0';
					p_rivi[p_paikka] = c_maara;
					p_paikka++;
					p_rivi[p_paikka] = ed_kirjain;
					p_paikka++;
					maara = 1;
				}
				else //Muuten pelkk� kirjain
				{
					p_rivi[p_paikka] = ed_kirjain;
					p_paikka++;
				}
			}
		}
	}

	//Kirjoitetaan pakattu tiedosto
	fwrite(p_rivi, sizeof(p_rivi), 1, pak_tiedosto);

	//Suljetaan tiedostot	
	fclose(tek_tiedosto);
	fclose(pak_tiedosto);
	
}
