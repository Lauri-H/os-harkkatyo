/*
Nimi: Lauri Heininen
Opiskelija nro: 0429765
Harjoitusty� teht�v� 1

L�hteet: -
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void lueTiedosto(char*);


int main(int argc, char* argv[])
{
	char* nimi;
	int i;
	
	//Jos argumentteja ei ole annettu, poistutaan
	if(argc <= 1)
	{
		exit(0);
	}
	
	//K�yd��n l�pi kaikki annetut tiedostojen nimet ja luetaan ne
	for(i = 1; i< argc; i++)
	{
		nimi = argv[i];
		lueTiedosto(nimi);
	}
	
	return 0;
}

//Funktio jolla luetaan annettu tiedosto.
//Parametrina tiedoston nimi
void lueTiedosto(char* nimi)
{
	//Avataan tiedosto
	FILE* tiedosto = fopen(nimi,"r");
	int c;

	//Testataan onnistuiko tiedoston avaus	
	if(tiedosto == NULL)
	{
		printf("my-cat: cannot open file\n");
		exit(1);
	}
	
	//Luetaan kirjain kerrallaan kunnes tullaan loppuun
	while((c = fgetc(tiedosto)) != EOF)
	{
		putchar(c);
	}
	
	//V�liin pari rivi� ja suljetaan tiedosto
	printf("\n\n");
	fclose(tiedosto);
	
}


