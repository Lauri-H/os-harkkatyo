my-cat tulostaa tiedoston sisällön terminaaliin.

my-cat toimii seuraavaavalla komennolla:
./my-cat filename1 filename2...

mikäli tiedostoa ei anneta ohjelma sulkeutuu normaalisti
mikäli tiedostoa ei voida avata jostain syystä ohjelma ilmoittaa virheestä

my-grep hakee annettua hakusanaa joko tiedostosta tai käyttäjän syötteestä ja tulostaa
löytyneet rivit, joilla sana esiintyy, terminaaliin

Kun haetaan käyttäjän syötteestä ohjelma ajetaan komennolla:
./my-grep hakusana

ja kun haetaan tiedostosta:
./my-grep hakusana tiedosto1 tiedosto2...

Ohjelma ilmoittaa seuraavista virheistä:
-Jos hakusanaa ei ole annettu
-Jos tiedostoa ei voida avata


my-zip pakkaa annetun tekstitiedoston binäärimuotoon käyttäen RLEN algoritmia.
ohjelma ajetaan komennolla: ./my-zip lähdetiedosto pakattu_tiedosto
lähdetiedoston tulee olla .txt ja pakattu tiedosto .bin

Ohjelma ilmoittaa, jos argumentteja on liikaa tai liian vähän


my-unzip purkaa my-zip ohjelmalla pakatun tiedoston.
ohjelma ajetaan komennolla ./my-unzip purettava_tiedosto purettu_tiedosto
purettava tiedosto tulee olla .bin ja purettu tiedosto tulee olemaan .txt

ohjelma ilmoittaa, jos argumentteja on liikaa tai liian vähän.
