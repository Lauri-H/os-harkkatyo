/*
Nimi: Lauri Heininen
Opiskelija nro: 0429765
Harjoitusty� teht�v� 1

L�hteet:
https://stackoverflow.com/questions/2279379/how-to-convert-integer-to-char-in-c
https://www.tutorialspoint.com/c_standard_library/string_h.htm
https://www.tutorialspoint.com/c_standard_library/c_function_fwrite.htm
https://en.wikipedia.org/wiki/C_standard_library
Linux manuaali mm. strcpy ja getline
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define riviKoko 2500

//Funktio esittely
void pura(char* bin_nimi, char* p_nimi);

int main(int argc, char* argv[])
{
	char* bin_nimi; //Luettavan bin��ri tiedoston nimi
	char* p_nimi; //Purettavan tiedoston nimi
	
	//tarkistetaan ett� argumentteja on oikea m��r�
	if(argc < 3)
	{
		perror("Liian v�h�n argumentteja.\n");
	}
	else if(argc > 3)
	{
		perror("Liikaa argumentteja");
	}
	else //Jos kaikki kunnossa niin aloitetaan
	{
		bin_nimi = argv[1];
		p_nimi = argv[2];
	}

	pura(bin_nimi,p_nimi);
	
	return 0;

}

//Aliohjelma joka purkaa tiedoston. Parametreina tiedostojen nimet
void pura(char* bin_nimi, char* p_nimi)
{
	int pituus;
	int i;
	int j;
	int p_paikka = 0;
	int i_ed_kirjain;
	char ed_kirjain;
	char seu_kirjain;
	char rivi[riviKoko];
	char p_rivi[riviKoko];
	
	//Avataan tiedostot
	FILE* bin_tiedosto = fopen(bin_nimi, "rb");
	FILE* pur_tiedosto = fopen(p_nimi, "w");

	//Luetaan koko tiedosto
	while(fgets(rivi, riviKoko, bin_tiedosto) != NULL)
	{
		pituus = strlen(rivi);
			
		//Suoritetaan kunnes rivi loppuu
		for(i= 1; i<=pituus; i++)
		{
			//M��ritet��n edellinen ja seuraava kirjain
			ed_kirjain = rivi[i-1];
			seu_kirjain = rivi[i];
			
			//Jos kirjain EI ole numero, kirjoitetaan
			if(isdigit(ed_kirjain) == 0)
			{
				p_rivi[p_paikka] = ed_kirjain;
				p_paikka++; 
			}
			else //Jos on, puretaan ensin
			{
				//Muutetaan kokonaisluvuksi
				i_ed_kirjain = ed_kirjain - '0';
				
				//Lis�t��n kirjaimia numeron osoittama m��r�
				for(j=1; j<i_ed_kirjain; j++)
				{
					p_rivi[p_paikka] = seu_kirjain;
					p_paikka++;
						
				}
			}
		}
	}	
	
	//Kirjoitetaan purettu tiedosto
	fwrite(p_rivi, 1, sizeof(p_rivi), pur_tiedosto);
	
	//Suljetaan tiedostot
	fclose(bin_tiedosto);
	fclose(pur_tiedosto);
}
