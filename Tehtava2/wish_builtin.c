#ifndef kirjastot
#define kirjastot
#include <sys/wait.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define polunPituus 30
#include "wish_builtin.h"
#endif

//Lista funktioiden nimist�
char* builtin_str[] =
{
	"wish_cd",
	"wish_exit",
};

//Lista funktioista
int (*builtin_func[]) (char**) =
{
	&wish_cd,
	&wish_exit,
};

//Funktio joka laskee ohjelmien m��r�n
int wish_builtins_maara()
{
	return sizeof(builtin_str) / sizeof(char*);
}

int wish_cd(char **argumentit)
{
	//Jos argumenttia ei ole, annetaan virhe
	if(argumentit[1] == NULL)
	{
		fprintf(stderr, "wish: toiminto \"cd\" vaatii argumentin.\n");
		return 1;
	}
	else //Muuten vaihdetaan kansiota argumentin mukaan ja tarkistetaan ett� se on olemassa
	{
		if(chdir(argumentit[1]) != 0)
		{
			perror("wish");
		}
	}

	return 1;
}

//Funktio shellin lopettamiseen
int wish_exit(char** args)
{
	exit(0);
}

//Funktio path muuttujan muokkaamiseen
char** wish_path(char** argumentit,char** osoitteet)
{
	int i;
	char uusi_polku[polunPituus];
	
	//Jos argumenttia ei annettu annetaan virhe
	if(argumentit[1] == NULL)
	{
		fprintf(stderr, "wish: toiminto \"path\" vaatii argumentin.\n");
		exit(1);
	}
	
	//Lis�t��n uusi osoite muuttujaan
	strcpy(uusi_polku, argumentit[1]);
	
	//Kelataan listan loppuun
	while(osoitteet[i] != NULL)
	{
		i++;
	}
	
	//Lis�t��n uusi osoite path muuttujan loppuun
	osoitteet[i+1] = uusi_polku; 
	return osoitteet;
}
