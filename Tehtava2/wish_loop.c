#ifndef kirjastot
#define kirjastot
#include <sys/wait.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


#define WISH_JAKO_BUFFERIKOKO 64
#define WISH_JAKO_JAKAJAT " \t\r\n\a"

#include "wish_builtin.h"
#include "wish_loop.h"
#endif

//Shellin looppi joka k�ynnistet��n mainissa.
void wish_loop(void)
{
	//M��ritet��n path muuttuja
	char* envPath[] = 
	{
		(char*) "PATH=/bin",
		0
	};
		
	char *rivi;
	char **argumentit;
	int tila;
	
	//Varsinainen looppi
	do
	{
		printf("wish> ");
		rivi = wish_lueRivi(); //Lue rivi komentorivilt�
		argumentit = wish_jaaRivi(rivi); //Jaa rivi palasiksi
		tila = wish_execute(argumentit, envPath); //Suorita

		//Vapauta muisti
		free(rivi);
		free(argumentit);
		
	}while(tila);

}

//Aliohjelma joka lukee tekstin komentorivilt�
char* wish_lueRivi(void)
{
	char* rivi = NULL;
	size_t bufferikoko = 0; //getline osaa varata oikean määrän itsekseen
	ssize_t luettu;	
	luettu = getline(&rivi, &bufferikoko, stdin);
	return rivi;
}

//Aliohjelma, joka jakaa rivin argumenteiksi
char** wish_jaaRivi(char* rivi)
{
	int bufferikoko = WISH_JAKO_BUFFERIKOKO;
	int paikka = 0;
	char** palikat;
	char* palikka;
	
	//Varataan muisti
	if((palikat = (char**)malloc(bufferikoko * sizeof(char*)))== NULL)
	{
		fprintf(stderr, "wish: virhe muistin varaamisessa\n");
		exit(EXIT_FAILURE);	
	}

	//Jaetaan rivi palasiin
	palikka = strtok(rivi, WISH_JAKO_JAKAJAT);	

	while(palikka != NULL)
	{	
		palikat[paikka] = palikka;
		paikka++;

		//Jos ollaan muistialueen lopussa, varataan lis��
		if(paikka >= bufferikoko)
		{
			bufferikoko += WISH_JAKO_BUFFERIKOKO;
		
			if((palikat = (char**) realloc(palikat, bufferikoko * sizeof(char*)))== NULL)
			{
				fprintf(stderr, "wish: virhe muistin varaamisessa\n");
				exit(EXIT_FAILURE);	
			}
		}
		palikka = strtok(NULL, WISH_JAKO_JAKAJAT);
	}
	
	//Lis�t�t��n terminator loppuun ja palautetaan argumentit
	palikat[paikka] = NULL;
	return palikat;

}

//Aliohjlema, joka suorittaa ohjelman 
int wish_aja(char** argumentit, char** evenPath)
{
	pid_t pid;
	pid_t wpid;
	int tila;
	char* ohjelma_nimi = argumentit[0];
	
	//Luodaan uusi prosessi
	pid = fork();

	if(pid == 0) //jos prosessi on lapsiprosessi
	{
		//Suoritetaan ohjelma j�rjestelm�komennolla
		if(execvpe(ohjelma_nimi, argumentit, evenPath) == -1)
		{
			perror("wish suorittamisessa virhe\n");
		}
		exit(EXIT_FAILURE);
	}
	else if(pid < 0) //Virhe forkissa
	{
		perror("wish prosessin luomisessa virhe\n");
	}
	else //Vanhempi prosessi
	{
		//Odotetaan kunnes lapsiprosessi terminoituu
		do
		{
			wpid = waitpid(pid, &tila, WUNTRACED);
		}while(!WIFEXITED(tila) && !WIFSIGNALED(tila));
	}
	return 1;	
}

//Aliohjelma, joka tarkistaa onko ajettava ohjelma sis��nrakennettu 
int wish_execute(char** argumentit, char** envPath)
{
	int i;

	//Tarkistetaan onko annettu suoritettavaa ohjelmaa
	if(argumentit[0] == NULL)
	{
		return 1;
	}
	
	//K�yd��n l�pi builtin ohjelmat
	for(i = 0; i < wish_builtins_maara(); i++)
	{
		//Jos suoritettava ohjelma on sis��nrakennettu, suoritetaan
		if(strcmp(argumentit[0], builtin_str[i]) == 0)
		{	
			return (builtin_func[i])(argumentit);
		}

		//Jos halutaan muuttaa path muuttujaa, suoritetaan
		if(strcmp(argumentit[0], "wish_path") == 0 && i == 2)
		{
			wish_path(argumentit, envPath);
			exit(0);
		}

	}

	//Jos ei ole sis��nrakennettu, siirryt��n suorittamaan se
	return wish_aja(argumentit, envPath);

}
