/*
Nimi: Lauri Heininen
Opiskelija nro: 0429675
Harjoitustyön tehtävä 2

lähteet:
- https://brennan.io/2015/01/16/write-a-shell-in-c/ valtaosa koodauksesta
- http://man7.org/linux/man-pages/man2/chdir.2.html
  Lis�ksi manuaalista:
  	j�rjestelm�komennot fork, execvpe.
  	kirjastofunktiot strcmp, strtok
*/
#include <sys/wait.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "wish_loop.h"

int main(int argc, char** argv)
{
	//Suoritetaan shellin looppia niin kauan kuin on tarvis
	wish_loop();

	//Lopetetaan ohjelma
	return 0;
}
