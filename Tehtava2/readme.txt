Shell ajetaan komennolla ./wish ilman parametreja

Shellissä on seuraavat sisäänrakennetut toiminnot:
wish_exit - poistuu shellistä
wish_cd [polku] - siirtyy mainittuun hakemistoon
wish_path [polku] - lisää polun PATH muuttujaan.

lisäksi shell tukee normaaleja linux komentoja, jotka löytyvät PATH muuttujan määrittelemistä
hakemistoista.

Shell ei tue rinnakkaisia komentoja tai uudelleenohjausta
