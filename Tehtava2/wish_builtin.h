#ifndef BUILTIN_H
#define BUILTIN_H


int wish_builtins_maara();
int wish_cd(char**);
int wish_exit(char**);
char** wish_path(char**,char**);

extern int (*builtin_func[]) (char**);
extern char* builtin_str[];


#endif
